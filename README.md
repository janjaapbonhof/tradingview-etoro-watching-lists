This is how you can screen your watch list:
1) Go to the watchlist you want to screen
2) Right-click in the watchlist and choose “Flag all symbols from this list only”
3) Go to the Screener and select “Show flagged symbols only” in the ticker column
4) You can now start screening the symbols from your watch list 
